\documentclass[a4paper,pdf,color,11pt]{CITnote}

\usepackage[margin=30mm]{geometry}
\usepackage{changepage}
\usepackage{amsmath}
\usepackage{booktabs}
\usepackage{siunitx}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[italian]{babel}
\usepackage{hyperref}
\usepackage{mwe}

\sisetup{
    separate-uncertainty,
}

\author{Davide De Prato \and
	Francesco Musso \and
	Shoichi Yip}
%\refnum{S2....}
\shorttitle{Esperienza 4}
\title{Esperienza 4: Interferometro Michelson}
\date{\today}
\issue{4}

\usepackage{tikz}
%\usetikzlibrary{arrows,shape,positioning}

\newcommand{\tcr}{\textcolor{red}}
\newcommand{\tcb}{\textcolor{blue}}
\newcommand{\tcm}{\textcolor{magenta}}
\newcommand{\tcg}{\textcolor{green}}
\newcommand{\tcp}{\textcolor{purple}}
\DeclareMathOperator\erf{erf}

\begin{document}

\maketitle

\begin{adjustwidth*}{1.5cm}{1.5cm}
\begin{center}
\section*{Abstract}
\end{center}

\bf{In questa esperienza si costruisce un interferometro di Michelson, al fine
di calcolare la lunghezza d'onda di un raggio laser e l'indice di rifrazione di
due lamine di plexiglass di diverso spessore. Si ottiene un valore sovrastimato
di $\lambda=\SI{578(13)}{\nano\meter}$ rispetto al valore nominale,
probabilmente dovuto ad un sistematico sulle misure. Invece gli indici di
rifrazione del plexiglass ($n_1=\SI{1.72(8)}{}$ e $n_2=\SI{1.51(1)}{}$)
risultano compatibili con il valore nominale.}

\end{adjustwidth*}

\section{Introduzione}

L'interferometro Michelson consiste in un \emph{beam splitter} che separa la
luce emessa da un diodo laser in due fasci perpendicolari di eguale intensità.
I due fasci vengono poi riflessi da due specchi situati a distanza $L_1$ ed
$L_2$ dal \emph{beam splitter}. In esso si ricombinano i due fasci che infine
generano delle frange di interferenza su uno schermo.

Dopo essere stati splittati i raggi percorrono due cammini rispettivamente
$2L_1$ e $2L_2$, dai quali dipende la figura di interferenza osservata. Questa
figura cambia al variare della posizione longitudinale di uno dei due specchi.

Se in un punto dello schermo si osserva un massimo, esso si ripresenterà nel
medesimo punto una volta che la differenza tra i due cammini sarà variata di
$k\lambda$, dove $\lambda$ è la lunghezza d'onda del raggio laser e k è un
intero, cioè ogniqualvolta la posizione dello specchio mobile sarà variata di
$k\frac{\lambda}{2}$

Per misurare $\lambda$ è dunque necessario fissare un punto sullo schermo,
spostare lo specchio mobile di una quantità $\Delta x$ e contare quante volte si
presenta un massimo nel punto fissato, secondo l'equazione
\begin{equation}
	\Delta x = N\dfrac{\lambda}{2}.
    \label{eq:lambda}
\end{equation} 

%Questa operazione è stata ripetuta in due diversi setup: nel secondo caso è
%stata aggiunta una lente convergente prima del beam splitter, in modo da
%modificare la forma del fascio ed ottenere una figura di diffrazione con frange
%circolari concentriche, anziché frange rettilinee.

L'interferometro di Michelson permette anche di calcolare l'indice di rifrazione
di un materiale posto tra il \textit{beam splitter} e uno specchio, studiando la variazione delle frange di
interferenza al variare dell'angolo $\alpha$ del materiale stesso.  In questo
esperimento si posiziona una lamina di plexiglass da lungo uno dei due bracci
dell'interferometro, inizialmete perpendicolare al raggio, poggiata su un
supporto rotante dotato di nonio angolare, in modo da poter regolare
l'inclinazione tra raggio e lamina.

L'operazione di misura è simile a quella della parte precedente. Si tratta di
contare gli $N$ massimi che passano per un punto fissato, al variare dell'angolo
del plexiglass rispetto all'inclinazione iniziale. La relazione esistente tra $N$ e l'anoglo $\alpha$ è la seguente:
\begin{equation}
	N = \frac{2t(1 - \cos(\alpha))(n-1)}{n\lambda - \lambda(1-\cos(\alpha)) },
    \label{eq:n_alpha}
\end{equation}
%Per ricavare l'indice di rifrazione $n_{\text{pg}}$ è necessario contare il numero $N$ di
%volte in cui si presanta un massimo in una data posizione dello schermo,
%variando l'angolo in un intervallo [$\angle{0} - \angle{15} ??? $].  Per il fit
%non so se é per il fit questa formula...  dei dati si \`{e} poi utilizzata la
%formula 
dove $t$ è lo spessore della lamina.

\label{sec:intro}

\section{Setup}

\subsection*{Materiali utilizzati}

\begin{itemize}
	\item banco ottico ($\SI{30}{\centi\meter}$ x $\SI{60}{\centi\meter}$);
	\item diodo laser ($\lambda=\SI{532}{\nano\meter}$,
			$P<\SI{1}{\milli\watt}$, Classe II);
		\item \emph{beam splitter} con prisma cubico;
	\item 2 specchi, di cui uno dotato di vite micrometrica in grado di muoversi in direzione perpendicolare alla superficie;
	\item supporto rotante dotato di nonio angolare;
	\item 2 lamine di plexiglass di spessori nominali $t_1^n = \SI{8}{\milli\meter}$ e $t_2^n = \SI{12}{\milli\meter}$;
	\item lente convergente biconvessa ($f=\SI{50}{\milli\meter}$);
	\item schermo.
\end{itemize}

\subsection*{Schema e impostazione}

\begin{figure}[h]
	\centering
	\input{schema.pdf_tex}
	\caption{Schema dell'interferometro di Michelson}
	\label{fig:schema}
\end{figure}

Come già descritto nella Sezione~\ref{sec:intro}, sul banco ottico vengono
installate le varie componenti come in Figura~\ref{fig:schema}. Questo fa sì che
la luce, incidendo per metà dell'intensità sullo specchio $M_1$ e per metà sullo
specchio $M_2$, si ricombini al \emph{beam splitter} e si formi una figura di
interferenza sullo schermo. Le frange di interferenza sono in prima
approssimazione lineari nell'impostazione senza lente convergente, sferiche
nell'impostazione con lente convergente.

\section{Descrizione dei risultati}

\begin{figure}[h]
	\centering
	\begin{minipage}{0.33\textwidth}
		\includegraphics[width=0.9\textwidth]{fotoschema.jpg}
		\caption{Foto dell'interferometro costruito in laboratorio}
	\end{minipage}
	\begin{minipage}{0.25\textwidth}
		\includegraphics[width=0.9\textwidth]{frange.jpg}
		\caption{Frange di interferenza lineari}
	\end{minipage}
	\begin{minipage}{0.25\textwidth}
		\includegraphics[width=0.9\textwidth]{frangec.jpg}
		\caption{Frange di interferenza circolari}
	\end{minipage}
	\label{fig:frangeinterf}
\end{figure}

Si procede con la stima di $\lambda$. Si effettua una misura di $N$ al variare
della posizione di uno specchio contando sia le frange lineari nella
configurazione senza lente che le frange circolari nella configurazione con la
lente. L'errore sul valore di $N$ è stimato come $\frac{1}{15}\,N$. Si
ottengono i rispettivi valori di $\lambda$ invertendo
l'equazione~\ref{eq:lambda}. Si calcola la media tra questi due valori e si
ottiene una stima di $\lambda$. In questo caso si ottiene:
\[
	\lambda = \SI{578(13)}{\nano\meter}
\]

	che non risulta compatibile in un intervallo di incertezza esteso di
$3\sigma$ con il valore nominale dichiarato dal costruttore del laser di
$\SI{532}{\nano\meter}$. La sovrastima di $\lambda$ è probabilemente causata dalla non perfetta perpendicolarità del raggio rispetto alla superficie degli specchi.
Per tale ragione, d'ora in poi, si prende in considerazione il valore di
$\lambda$ indicato dal costruttore.

Come descritto nella Sezione~\ref{sec:intro}, si studia l'andamento di $N$ in
funzione dell'angolo $\alpha$ di inclinazione della lamina di plexiglass al fine
di determinarne l'indice di rifrazione $n$. Si effettuano due coorti di misure
di $N$ al variare di $\alpha$: la prima con una lamina di spessore nominale
$t^n_1 = \SI{8}{\milli\meter}$ ($\SI{7.80(5)}{\milli\meter}$), mentre la seconda di spessore nominale $t^n_2 =
\SI{12}{\milli\meter}$ ($\SI{11.90(5)}{\milli\meter}$). In entrambi i casi si utilizza l'apparato sperimentale
nella configurazione con la lente dato che le frange circolari risultano
più facili da contare rispetto a quelle lineari.

Si esegue un fit con i valori delle misure sull'equazione~\ref{eq:n_alpha}.
Vengono riportati in Figura~\ref{fig:t1} e in Figura~\ref{fig:t2}
rispettivamente i dati e il fit nel caso con la lamina di spessore $t_1$ e con
spessore $t_2$. Viene in oltre riportata l'andamento della funzione $N(\alpha)$
con il valore teorico di rifrazione del plexiglass $n = 1.48$.

\begin{figure}[p]
    \centering
    \includegraphics[page=1,width=.8\textwidth]{multipage.pdf}
    \caption{Dati sperimentali e fit con la lamina plexiglass di spessore $t_1$}
    \label{fig:t1}
\end{figure}

\begin{figure}[p]
    \centering
    \includegraphics[page=2,width=.8\textwidth]{multipage.pdf}
    \caption{Dati sperimentali e fit con la lamina plexiglass di spessore $t_2$}
    \label{fig:t2}
\end{figure}

Otteniamo i seguenti valori di $n$:
\[
n_1 = \SI{1.72(8)} \quad n_2 = \SI{1.51(1)}.
\]
Entrambi i valori risultano compatibili in un intervallo di incertezza esteso
$3\sigma$ con il valore teorico di $n = 1.48$. Nel secondo caso la stima sembra
migliore in quanto l'errore relativo percentuale risulta essere molto minore del
primo ($\simeq 0.7 \%$ contro il $\simeq 5 \%$ del primo caso). Osservando i
grafici si nota inoltre come nel secondo caso il fit segua meglio l'andamento
teorico e come approssimi meglio i dati anche per angoli maggiori di $\ang{10}$.

\section{Conclusione}

Si è verificata la compatibilità dei valori dell'indice di rifrazione dei
plexiglass sperimentali
\[
	n_1 = \SI{1.72(8)}{} \qquad n_2 = \SI{1.51(1)}{}
\]
con il valore nominale di $n=1.48$.

Vi è invece un'incompatibilità tra il valore di $\lambda$ sperimentale, che
risulta sovrastimato ($\lambda=\SI{578(13)}{\nano\meter}$) rispetto al valore
nominale $\lambda_n = \SI{532}{\nano\meter}$. L'impossibilità di verificare la perpendicolarità dei raggi rispetto alla superfici riflettenti degli specchi è probabilmente la causa di questa sovrastima.

\newpage

\section{Interferenza non ideale}

Un fascio con profilo d'intensità gaussiano, corrispondente al modo trasverso
fondamentale o $\text{TEM}_{00}$, è descritto dall'equazione generale
\begin{equation}
	\psi(r,z) = \frac{\omega_0}{\omega(z)} \,
	\exp\left(-\frac{r^2}{\omega^2(z)}\right) \,
	\exp\left(-i \frac{kr^2}{2 R(z)}\right) \, \exp\left(i\,
	\tan^{-1}\left(\frac{z}{z_R}\right)\right) \, \exp\left(-ikz\right).
	\label{eq:tem00}
\end{equation}

Ricordiamo inoltre la definizione di $q$ come
\begin{equation}
	\frac{1}{q} = \frac{1}{R} - i \frac{\lambda}{\pi\omega^2(z)}.
	\label{eq:qdef}
\end{equation}

Possiamo studiare il contrasto della figura di interferenza calcolando, con
l'integrale di \emph{overlap}, la potenza dovuta ai due fasci come
\begin{equation}
	P = \int_{-\infty}^{+\infty} \int_{-\infty}^{+\infty} \psi_1 \cdot \psi_2^*
	\, dx \, dy.
	\label{eq:overlapint}
\end{equation}

La potenza massima
\begin{equation}
	P_{\text{max}} = \frac{1}{\omega_1 \, \omega_2} \frac{4}{ik \left(
	\frac{1}{q_2^*} - \frac{1}{q_1} \right)}
	\label{eq:pmax}
\end{equation}
è data dalla condizione di normalizzazione applicata
all'Equazione~\ref{eq:overlapint}.

Pertanto si può trovare la potenza persa tra le due configurazioni studiate
(massimo contrasto con bracci uguali, e contrasto ridotto con un braccio che
misura \SI{3}{\centi\meter} meno) come
\begin{equation}
	P^\%_p = \frac{P_{\max} - P}{P_{\max}}.
	\label{eq:potperclost}
\end{equation}

\end{document}
