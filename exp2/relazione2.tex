\documentclass[pdf,color,11pt]{CITnote}
%optional [color] for a color logo

\usepackage[margin=30mm]{geometry}
\usepackage{changepage}
\usepackage{siunitx}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[italian]{babel}
\usepackage{hyperref}

\sisetup{
    separate-uncertainty,
}

\author{Davide De Prato \and
	Francesco Musso \and
	Shoichi Yip}
%\refnum{S2....}
\shorttitle{Esperienza 2}
\title{Esperienza 2: Telescopio terrestre e telescopio astronomico}
\date{\today}
\issue{2}

\usepackage{tikz}
%\usetikzlibrary{arrows,shape,positioning}

\newcommand{\tcr}{\textcolor{red}}
\newcommand{\tcb}{\textcolor{blue}}
\newcommand{\tcm}{\textcolor{magenta}}
\newcommand{\tcg}{\textcolor{green}}
\newcommand{\tcp}{\textcolor{purple}}

\begin{document}

\maketitle

%\section{la}

	%\addtolength{\oddsidemargin}{.875in}
	%\addtolength{\evensidemargin}{.875in}
	
\begin{adjustwidth*}{1.5cm}{1.5cm}
\begin{center}
\section*{Abstract}
\end{center}

{\bf{In questo esperimento si studiano le proprietà di lenti, telescopi
		astronomici e terrestri. Mediante la misura di lunghezze tra gli
		elementi sull'asse ottico è possibile ricavare la lunghezza focale del
		cristallino impiegato ($f=\SI{18.9(4)}{\centi\meter}$). Si costruisce un
		telescopio
		terrestre usando una lente divergente, ed un telescopio astronomico usando una lente convergente. Essi hanno rispettivamente
		ingrandimenti visuali $G_v^{\text{terr}}=\SI{5.3(1)}{}$ e
		$G_v^{\text{astr}}=\SI{9.8(1)}{}$.
}}\\

\end{adjustwidth*}

\section{Introduzione}

\begin{figure}[h]
	\centering
	\resizebox{75mm}{!}{\input{schemi_1.pdf_tex}}
	\caption{Schema illustrativo della prima parte dell'esperienza}
	\label{fig:schema_fotodiodo}
\end{figure}

Data un'impostazione come in Figura~\ref{fig:schema_fotodiodo} le misure della
distanza fra la sorgente luminosa e la lente $p$ e fra la lente e lo schermo $q$
ci danno una stima della focale $f$. Infatti sono legate dalla relazione:

\begin{equation}
	\frac{1}{q} + \frac{1}{p} = \frac{1}{f},
	\label{eq:pqrel}
\end{equation}
per cui si può ricavare $f$ come pendenza della retta data dalla regressione
lineare sul modello
\begin{equation}
	(p+q)\, f = pq.
	\label{eq:pqmodel}
\end{equation}

\begin{figure}[h]
	\centering
	\resizebox{75mm}{!}{\input{schemi_2.pdf_tex}}
	\caption{Schema illustrativo del modello utilizzato per le misure di
	$\alpha$ e $\beta$}
	\label{fig:schemi_telescopi}
\end{figure}

I telescopi sono costruiti come in Figura~\ref{fig:schemi_telescopi}.
Nel \emph{telescopio astronomico} abbiamo una sorgente a distanza infinita, e se
si posiziona l'oculare in modo che $F_{ob}\equiv F_{oc}$ si ottiene un'immagine nitida
sullo schermo. Tale immagine avrà un ingrandimento visuale pari a
\begin{equation}
	G_v^{\text{astr}} = \frac{\beta_1}{\alpha},
	\label{eq:gvastr}
\end{equation}
dove $\beta_1$ è l'angolo sotto il quale si vede l'immagine e $\alpha$ è
l'angolo sotto cui si vede l'oggetto a occhio nudo.
Si nota che le immagini per questo telescopio risultano capovolte, condizione
non ottimale per le osservazioni con un telescopio di tipo terrestre. Pertanto
nel \emph{telescopio terrestre} si impiega una lente di tipo divergente invece
che convergente. Analogamente al calcolo precedente
\begin{equation}
	G_v^{\text{terr}} = \frac{\beta_2}{\alpha}.
	\label{eq:gvastr}
\end{equation}

\section{Setup}

\begin{itemize}
	\item asse ottico;
	\item fotodiodo;
	\item torcia elettrica;
	\item cristallino, lente convergente e lente divergente;
	\item carta millimetrata;
    \item metro (risoluzione $\SI{1}{\milli\meter}$);
    \item calibro a vernio.
\end{itemize}

Per trovare la focale $f$ si installano sull'asse ottico il cristallino e il
fotodiodo, in modo da avere l'immagine dell'oggetto luminoso su uno schermo
ricoperto di carta millimetrata. Si prendono dieci misure di lunghezza diverse,
distribuite sull'intero asse ottico. Per ognuna si verifica che la
configurazione dia un'immagine nitida sul foglio millimetrato.

Successivamente si utilizza un'altra sorgente di luce (una torcia elettrica)
posta molto lontana dall'asse ottico. Si verifica, come già fatto con il
fotodiodo, la posizione del cristallino in cui l'immagine sullo schermo è nitida
e si registrano la distanza del cristallino dallo schermo $q$ e l'altezza
dell'immagine sullo schermo $y$, con l'aiuto rispettivamente di un righello e
di un calibro. L'uso di questo strumento è dovuta alla dimensione molto ridotta
di queste immagini, che richiedono dunque una misura più precisa.

Con la distanza $q$ fissata, si eseguono due misure di altezza: quella di $y_2$
con l'installazione di una lente convergente e quella di $y_1$ con
l'installazione di una lente divergente. Questo ci permette poi, come descritto
nella Sezione~\ref{sec:results}, di studiare gli angoli visuali e gli
ingrandimenti.

\label{sec:setup}

\section{Descrizione dei risultati}

\begin{figure}[h]
	\centering
	\includegraphics[page=1,width=.7\textwidth]{multipage.pdf}
	\caption{Dati e fit per le misure di distanza $p$ e $q$}
	\label{fig:meas_pq}
\end{figure}

Le dieci misure di distanza sono state effettuate sul metro corredato all'asse
ottico. Il fit lineare eseguito sui dati permette di stimare la focale a
$f=\SI{18.9(4)}{\centi\meter}$, compatibile entro $3 \sigma$ con il valore nominale di
$\SI{20}{\centi\meter}$.

Le seguenti misure effettuate con il calibro sono
$$
q=\SI{20.610(6)}{\centi\meter}
$$
$$
y=\SI{0.120(6)}{\centi\meter}
$$
$$
y_1=\SI{1.120(6)}{\centi\meter}
$$
$$
y_2=\SI{0.640(6)}{\centi\meter}
$$

Da queste deduciamo i seguenti valori di $\alpha$ e $\beta$ come
$\alpha=\arctan{\frac{y}{q}}$, $\beta_1=\arctan{\frac{y_1}{q}}$ e
$\beta_2=\arctan{\frac{y_2}{q}}$:
$$
\alpha = \SI{0.0058(1)}{\radian} \qquad \beta_1 = \SI{0.0572(1)}{\radian} \qquad \beta_2 = \SI{0.0310(1)}{\radian}.
$$

Gli ingrandimenti valgono, rispettivamente
$$
G_v^{\text{terr}} = \frac{\beta_1}{\alpha} = \SI{5.3(1)}{} \qquad
G_v^{\text{astr}} = \frac{\beta_2}{\alpha} = \SI{9.8(1)}{}
$$

\label{sec:results}

\section{Conclusione}

Si è verificata la compatibilità della misura di $f=\SI{18.9(4)}{\centi\meter}$ entro
$3\sigma$ con il valore nominale della focale di $\SI{20}{\centi\meter}$. È stato rilevato, dalle
misure di $y_1$, $y_2$, $y$ e $q$ che i telescopi terrestre e astronomico hanno
rispettivamente angoli visuali $beta_1 = \SI{0.0572(1)}{\radian}$ e $\beta_2=\SI{0.0310(1)}{\radian}$, e ingrandimenti visuali
$G_v^{\text{terr}}=\SI{5.3(1)}{}$ e
$G_v^{\text{astr}}=\SI{9.8(1)}{}$.

%Si riassumono i risultati ottenuti con la metodica usata. Molto simile all' abstract
%ma piu' concentrati sul finale. Un minimo di discussione dei risultati ed eventuali problematiche 
%trovate. (10/15 righe al massimo)

\end{document}
