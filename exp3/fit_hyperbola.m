%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FIT FUNCTION w(z) = w0*sqrt(1+(z-z0)^2/z_R^2) to data
%
% Script that computes the waist width 'w0' and position 'z0' of a gaussian 
% beam as described in the laboratory session.
%
% Fill the vectors w and z with you data (in SI units, aka meters) and the 
% script will output the fit parameters 'w0' and 'z0' in the vector 'P',
% which stands for Parameters.
%
% The uncertainties corresponding to the desired confidence level 'conf_lvl'
% are stored in 'err_P', which stands for error on Parameters. 
%
% AUTHOR: Davide Dal Bosco, 16/05/19
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

lambda = 532.4*10^-9;       % wavelength of laser
conf_lvl = 0.000002827;          % confidence level for the fit parameters

% Insert your data (use SI units the way God intended)

%16 1.8869
%47 2.031
%70 2.105
%101 2.227
%154 2.441

% The widths of the beam you measured
%w = [2.0362, 2.0794, 2.2861, 2.5432, 2.6451]*10^-3;      % Meters
w = [1.8869, 2.031, 2.105, 2.227, 2.441]*10^-3
% Add the uncertainties to make a weighted fit
err_w = [];                                              % Meters

% The positions corresponding to the widths above
%z = [9.3, 25.3, 70.3, 131.3, 153.3]*10^-2;               % Meters
z = [16.1, 47.1, 70.1, 101.1, 154.1]*10^-2; 
% FIT
% Model definition. 'P' is the vector for the fit parameters
F = @(P, xdata)(P(1)*sqrt(1+((xdata-P(2))/(pi*P(1)^2/lambda)).^2));

%Initial guesses
initial = [10^-4, -5];

% I can use the uncertainties on my data to weight the fit. A common choice
% is 'weights = 1/err_y^2'. I don't have such uncertainties so I consider
% all the weight equal to 1, which is the same as not weighting the data at
% all
weights = ones(1,length(w)); 
% Alternatively if you have the uncertainties on 'w', use 
% weights = err_w.^-2;

%Actual fit
[P, R, J, CovB, MSE, ErrorModelInfo] = nlinfit(z, w, F, initial, ...
    'Weights', weights);

% Uncertainty on the fit paramters from the function 'nlparci'
ci = nlparci(P,R,'jacobian',J, 'Covar', CovB, 'alpha', 1-conf_lvl);
err_P = [(ci(1,2)-ci(1,1))/2, (ci(2,2)-ci(2,1))/2];

% Plot figure
% Define a reasonable x-vector for the plot
z_plot = linspace(-2*P(2), 2*P(2), 1000);
figure()
plot(z_plot, F(P,z_plot), 'LineWidth', 2)
hold on
scatter(z, w, 'LineWidth',1.5)
title('Beam width $w$ vs. knife distance $z$',  'Interpreter', 'latex')
xlabel('Knife distance $z$ [m]', 'Interpreter', 'latex')
ylabel('Beam width $w$ [m]', 'Interpreter', 'latex')
legend({['Fit: $w_0=$', num2str(P(1)), '$\pm$', num2str(err_P(1)), ...
    ' m, $z_0=$', num2str(P(2)), '$\pm$', num2str(err_P(2)), ' m'], ...
    'Data'}, 'Interpreter', 'latex')
grid on
box on