\documentclass[pdf,color,11pt]{CITnote}
%optional [color] for a color logo

\usepackage[margin=30mm]{geometry}
\usepackage{changepage}
\usepackage{amsmath}
\usepackage{booktabs}
\usepackage{siunitx}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[italian]{babel}
\usepackage{hyperref}

\sisetup{
    separate-uncertainty,
}

\author{Davide De Prato \and
	Francesco Musso \and
	Shoichi Yip}
%\refnum{S2....}
\shorttitle{Esperienza 3}
\title{Esperienza 3: Misura del profilo longitudinale del fascio gaussiano}
\date{\today}
\issue{3}

\usepackage{tikz}
%\usetikzlibrary{arrows,shape,positioning}

\newcommand{\tcr}{\textcolor{red}}
\newcommand{\tcb}{\textcolor{blue}}
\newcommand{\tcm}{\textcolor{magenta}}
\newcommand{\tcg}{\textcolor{green}}
\newcommand{\tcp}{\textcolor{purple}}
\DeclareMathOperator\erf{erf}

\begin{document}

\maketitle

\begin{adjustwidth*}{1.5cm}{1.5cm}
\begin{center}
\section*{Abstract}
\end{center}

{\bf{In questa esperienza si studiano le proprietà che caratterizzano
un fascio con profilo di intensità gaussiano, $\omega$ e $z_0$, emesso
da un diodo laser. Viene impiegata
la \textit{knife-edge technique}, che consiste nel coprire progressivamente
la sezione trasversale del raggio, incidente su un fotodiodo per misurarne
l'intensità. Si misurano $\omega_0 = \SI{0.57(2)}{\milli\meter}$ e
$z_0 = \SI{6.0(3)}{\meter}$.}}\\

\end{adjustwidth*}

\section{Introduzione}

% fase di allineamento

\begin{figure}[h]
	\centering
	\includegraphics[scale=.7]{gauss_lat.pdf}
	\caption{Profilo di un fascio gaussiano}
	\label{fig:gauss_lat}
\end{figure}

Un fascio emesso da un diodo laser può essere approssimato ad un fascio con
profilo d'intensità gaussiano, che corrisponde al modo $\text{TEM}_{00}$.

L'esperienza di laboratorio consiste nel misurare i valori di
$V_{\text{exp}}$ dati dal voltmetro connesso al fotorivelatore al variare della
posizione $x$ di uno schermo nero, che progressivamente copre trasversalmente il
fascio (\textit{knife-edge technique}). Ciò permette di misurare il raggio e la posizione del waist del fascio emesso dal nostro diodo laser.  Il
modello prevede che la differenza di potenziale dovuta al fotorivelatore,
direttamente proporzionale all'intensità del fascio, sia
\begin{equation}
	V_{\text{exp}} = \frac{V_{\text{max}}}{2} \, \left[ 1 - \erf \left(
	\sqrt{2} \, \frac{x - x_0}{\omega} \right) \right],
	\label{eq:model_lat}
\end{equation}
dove $V_{\text{max}}$ è la differenza di potenziale massima, $\erf$ è la
funzione tale che
\begin{equation}
	\erf(\cdot) = \frac{2}{\sqrt{\pi}} \int_0^\cdot e^{-t^2}\, dt,
	\label{eq:erfdef}
\end{equation}
$x$ e $x_0$ sono rispettivamente gli spostamenti dello schermo nero misurati sul
micrometro e il centro del fascio, e $\omega$ è il raggio
$1/e^2$, definito come
\begin{equation}
	\omega(z) = \omega_0 \sqrt{1 + \frac{z^2}{z_R^2}}.
	\label{eq:omegaz}
\end{equation}

Eseguendo un fit
all'Equazione~\ref{eq:model_lat} sui dati sperimentali si trova una stima di
$\omega$ per ciascun valore di $z$. Da cinque coorti di misure di
$V_{\text{exp}}$ è possibile estrarre cinque valori di $\omega(z)$ e da esse si
esegue un fit al modello all'Equazione~\ref{eq:omegaz}.

\label{sec:intro}

\section{Setup}

\subsection*{Materiali utilizzati}

\begin{itemize}
	\item asse ottico;
	\item diodo laser ($\lambda=\SI{532}{\nano\meter}$,
			$P<\SI{1}{\milli\watt}$, Classe II);
	\item fotodiodo al Silicio;
	\item voltmetro (risoluzione \SI{0.1}{\volt} per
		$V\geq\SI{1}{\volt}$ e \SI{0.1}{\milli\volt} per $V<\SI{1}{\volt}$);
	\item lente convergente;
	\item metro (risoluzione \SI{1}{\milli\meter})
	\item lamina scura libera di muoversi trasversalmente collegata ad un
		micrometro (risoluzione \SI{0.01}{\milli\meter}).
\end{itemize}

\subsection*{Schema e impostazione}

\begin{figure}[h]
	\centering
	\input{schema.pdf_tex}
	\caption{Schema del setup}
	\label{fig:schema}
\end{figure}

\label{sec:setup}

Si dispongono sull'asse ottico il laser con apposito aggancio, la lente
convergente, lo schermo nero installato ad un micrometro ed il fotorivelatore. Si
verifica l'allineamento di tutta la strumentazione sincerandosi che il fascio
laser incidente sul fotodiodo si rifletta esattamente nell'origine del fascio
stesso.

Questa disposizione permette di far sì che si possano effettuare le coorti di
misure per diversi valori di $z$ spostando semplicemente l'aggancio del
micrometro e rilevandone la posizione sul metro dell'asse ottico.

Successivamente si collega il fotorivelatore ad un voltmetro, in modo da
rilevare una differenza di potenziale proporzionale all'intensità della
radiazione incidente sul dispositivo. I fotoni che incidono sul fotodiodo fanno
sì che il diodo, polarizzato inversamente, generi una corrente continua.

\section{Descrizione dei risultati}

\begin{figure}[p]
	\centering
	\includegraphics[page=1,width=.8\textwidth]{multipage.pdf}
	\caption{Dati di differenza di potenziale rilevata in funzione dello
	spostamento dello schermo sul micrometro, fit per il modello
	all'Equazione~\ref{eq:model_lat} e residui}
	\label{fig:curve_modellat}
\end{figure}

\begin{figure}[p]
    \centering
    \includegraphics[page=2,width=.8\textwidth]{multipage.pdf}
    \caption{Dati di $\omega(z)$, fit all'Equazione~\ref{eq:omegaz} e residui}
    \label{fig:omegaz_curve}
\end{figure}

Si verifica la compatibilità del modello all'Equazione~\ref{eq:model_lat} con i
dati sperimentali in tutte e cinque le coorti di misure di $V_{\text{exp}}$. Si
può osservare una tendenza dei residui di oscillare attorno allo zero al variare
dello spostamento col micrometro.

È probabile che questo errore sistematico sia dovuto ad anomalie nella
rilevazione del fotodiodo: in particolare è possibile che la superficie di
incidenza del raggio non abbia la stessa responsività nelle sue varie parti che la
compongono. Si può ipotizzare che, dato l'andamento dei residui in
Figura~\ref{fig:curve_modellat}, il recettore sia costituito da bande circolari
concentriche a responsività alternatamente alta e bassa.


\begin{table}
	\begin{tabular}{cccc}
		\toprule
		$z$ & $V_{\text{max}}$ & $x_0$ & $\omega$\\
		\midrule
		\SI{0.000(1)}{\meter} & \SI{8.817(9)}{\volt} & \SI{2.632(2)}{\milli\meter} & \SI{1.899(6)}{\milli\meter}\\
		\SI{0.380(1)}{\meter} & \SI{8.778(8)}{\volt} & \SI{3.152(2)}{\milli\meter} & \SI{1.991(6)}{\milli\meter}\\
		\SI{0.750(1)}{\meter} & \SI{8.740(4)}{\volt} & \SI{4.545(2)}{\milli\meter} & \SI{2.093(4)}{\milli\meter}\\
		\SI{1.040(1)}{\meter} & \SI{8.732(5)}{\volt} & \SI{4.698(2)}{\milli\meter} & \SI{2.199(6)}{\milli\meter}\\
		\SI{1.330(1)}{\meter} & \SI{8.727(6)}{\volt} & \SI{4.776(3)}{\milli\meter} & \SI{2.278(7)}{\milli\meter}\\
		\bottomrule
		\end{tabular}
		\caption{Parametri del fit in corrispondenza dei diversi valori di $z$}
	\label{tab:zomega}
\end{table}

Si esegue il fit sui valori di $\omega$ in funzione della posizione sull'asse
ottico $z$, prendendo a modello l'Equazione~\ref{eq:omegaz}. Otteniamo i
seguenti valori
\[
	\omega_0 = \SI{0.57(2)}{\milli\meter},
\]
\[
	z_0 = \SI{-6.0(3)}{\meter}.
\]

\label{sec:results}

\section{Conclusione}

%Si riassumono i risultati ottenuti con la metodica usata. Molto simile all' abstract
%ma piu' concentrati sul finale. Un minimo di discussione dei risultati ed eventuali problematiche 
%trovate. (10/15 righe al massimo)

I valori $\omega_0=\SI{0.57(2)}{\milli\meter}$ e $z_0 = \SI{-6.0(3)}{\meter}$ caratterizzano il fascio laser, nell'approssimazione gaussiana. $z_0$ è negativo, e indica che il waist si trova in posizione retrostante al diodo laser impiegato.

È stata rilevato un andamento non casuale nei residui delle cinque coorti di dati $V_{\text{exp}}(x)$: si ritiene che tale comportamento sia dovuto ad una disomogeneità nella responsività del fotodiodo tra bande diverse della superficie di incidenza del raggio su di esso.

\label{sec:conclusion}

\newpage
\section{Fascio gaussiano attraverso lenti sottili}

\begin{figure}[h]
	\centering
	\includegraphics[width=.9\textwidth]{gausspropag.pdf}
	\caption{Curva $1/e^2$ della propagazione del fascio gaussiano in due lenti
	sottili}
	\label{fig:gausspropag}
\end{figure}

Per trovare le configurazioni tali che, dato un fascio gaussiano con waist
$\omega_0 = \SI{300}{\micro\meter}$ in $z_0=0$ e due lenti sottili di focale
$f_1$ ed $f_2$ distanti tra loro $L_2$ e rispettivamente distanti dal waist del
fascio entrante e dal waist del fascio uscente $L_1$ ed $L_2$, si abbia il waist
del fascio uscente $\omega_f = \SI{680}{\micro\meter}$ a $\SI{10}{\meter}$ da
$z_0$, si deve trovare la matrice ABCD che rappresenta il percorso del fascio.
Il calcolo è il seguente
\begin{equation}
	\left[ \begin{matrix} q\\1 \end{matrix} \right] = 
	\left[\begin{matrix}1 & L_{3}\\0 & 1\end{matrix}\right]
	\left[\begin{matrix}1 & 0\\- \frac{1}{f_{2}} & 1\end{matrix}\right]
	\left[\begin{matrix}1 & L_{2}\\0 & 1\end{matrix}\right]
	\left[\begin{matrix}1 & 0\\- \frac{1}{f_{1}} & 1\end{matrix}\right]
	\left[\begin{matrix}1 & L_{1}\\0 & 1\end{matrix}\right]
	\left[ \begin{matrix} q_0\\1 \end{matrix} \right].
	\label{eq:matmult}
\end{equation}

Eseguendo tutte le moltiplicazioni successive fra matrici e dividendo le due
righe della matrice risultante otteniamo la seguente espressione per $q$
\begin{equation}
	q = \frac{- L_{1} f_{1} f_{2} + L_{2} f_{2} \left(L_{1} - f_{1}\right) +
		\left(L_{3} + q_{0}\right) \left(L_{1} f_{1} + L_{1} f_{2} + L_{2}
	\left(- L_{1} + f_{1}\right) - f_{1} f_{2}\right)}{L_{2} f_{2} - f_{1} f_{2}
	+ \left(L_{3} + q_{0}\right) \left(- L_{2} + f_{1} + f_{2}\right)}
\label{eq:qfinale}
\end{equation}

Questa equazione ha quattro incognite, quindi abbiamo $\infty^3$
soluzioni. Per avere una determinata soluzione numerica si devono fissare altre
due incognite. Ad esempio, per semplificare i calcoli possiamo fissare
$L_1 = f_1$ e $L_2=f_1+f_2$. Si definisce
\begin{equation}
	\frac{1}{q} = \frac{1}{R} - i\frac{\lambda}{\pi\omega^2}.
	\label{eq:qdef}
\end{equation}
Sia $q$ che $q_0$ sono presi sul waist, hanno raggio di curvatura
infinito e perciò hanno parte reale nulla.
Se esplicitiamo $q$ e $q_0$ e risolviamo l'Equazione~\ref{eq:qfinale} otteniamo
\begin{equation}
	\begin{cases}
		\omega_f = \omega_0 \frac{f_1}{f_2}\\
		f_1+f_2 = \SI{5}{\meter}
	\end{cases}.
	\label{eq:fsolut}
\end{equation}

Date le condizioni iniziali troviamo che, affinché il waist del fascio uscente
sia in posizione $z_f=\SI{10}{\meter}$ con $\omega_f=\SI{680}{\micro\meter}$, con
le sopraindicate lunghezze $L_1$ ed $L_2$, le focali devono valere
\[
	f_1 = \SI{1.5306122}{\meter},
\]
\[
	f_2 = \SI{3.4693878}{\meter}.
\]

\end{document}
