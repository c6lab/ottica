\documentclass[pdf,color,11pt]{CITnote}
%optional [color] for a color logo

\usepackage[margin=30mm]{geometry}
\usepackage{changepage}
\usepackage{siunitx}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[italian]{babel}
\usepackage{hyperref}

\sisetup{
    separate-uncertainty,
}

\author{Davide De Prato \and
	Francesco Musso \and
	Shoichi Yip}
%\refnum{S2....}
\shorttitle{Esperienza 1}
\title{Esperienza 1: Indice di rifrazione}
\date{\today}
\issue{1}

\usepackage{tikz}
%\usetikzlibrary{arrows,shape,positioning}

\newcommand{\tcr}{\textcolor{red}}
\newcommand{\tcb}{\textcolor{blue}}
\newcommand{\tcm}{\textcolor{magenta}}
\newcommand{\tcg}{\textcolor{green}}
\newcommand{\tcp}{\textcolor{purple}}

\begin{document}

\maketitle

%\section{la}

	%\addtolength{\oddsidemargin}{.875in}
	%\addtolength{\evensidemargin}{.875in}
	
\begin{adjustwidth*}{1.5cm}{1.5cm}
\begin{center}
\section*{Abstract}
\end{center}

{\bf{
In questa esperienza si verifica la validità delle Leggi di Snell, studiando
come avviene la rifrazione di un raggio luminoso prodotto da un diodo laser
attraverso aria, acqua e olio. Preso l'indice di rifrazione dell'aria come
riferimento, si stimano gli indici per l'acqua ($n_{H_2O}=\SI{1.32(1)}{}$) e per l'olio ($n_{\text{olio}}=\SI{1.60(2)}{}$). Data la geometria
dell'apparato sperimentale non è stato possibile trovare l'angolo di riflessione
totale, ma se ne può trovare una stima a posteriori ($\theta_i^{\text{crit}}=\ang{46.06}$).
}}\\

\end{adjustwidth*}

\section{Introduzione}

Quando la luce si propaga in due mezzi contigui dalle proprietà chimiche
differenti, a livello della superficie di separazione tra i mezzi il fascio si
suddivide in un raggio riflesso ed un raggio rifratto. Il fenomeno che viene
studiato in questa esperienza è la rifrazione attraverso una lamina rifrangente
a facce di plexiglas piane contenente acqua e olio, immersa in aria.

\begin{figure}[h]
	\centering
	\resizebox{75mm}{!}{\input{schema_lastra.pdf_tex}}
	\caption{Schema illustrativo del modello utilizzato}
	\label{fig:schema_lastra}
\end{figure}

La legge che si vuole verificare è la seguente:
\begin{equation}
	x = t \, \sin{\theta_i} \left[ 1 -
	\sqrt{\frac{1-\sin^2{\theta_i}}{n^2_{12}-\sin^2{\theta_i}}} \right],
	\label{eq:fitmodel}
\end{equation}
dove $t$ è lo spessore della lamina, $\theta_i$ è l'angolo di incidenza del
raggio rispetto alla perpendicolare alla superficie della lamina, 
$n_{12}=\frac{n_2}{n_1}$ è
l'indice di rifrazione del mezzo 2 rispetto al mezzo 1 e $x$ è lo spostamento
del raggio rifratto rispetto al raggio incidente.

Vale la seguente approssimazione per raggi parassiali ($\theta_i\to0)$:
\begin{equation}
	x = t\, \theta_i \left( 1 - \frac{n_1}{n_2} \right).
	\label{eq:liltheta}
\end{equation}

\section{Setup}

\begin{itemize}
	\item diodo laser ($\SI{532}{\nano\meter}$, $P<\SI{1}{\milli\watt}$, Classe
		II)
	\item vaschetta di vetro riempita di acqua e olio;
	\item carta millimetrata;
    \item calibro (risoluzione $\SI{50}{\micro\meter}$)
	\item nonio ad arco;
	\item nonio lineare.
\end{itemize}

Si imposta il nonio, fissato al diodo laser, in modo che il raggio
attraversi la vasca collocata di fronte. Si installa il foglio di carta
milimetrata in senso orizzontale e opportunamente rialzato, in modo da
intercettare la luce rifratta dal liquido o direttamente uscente dal diodo.

Successivamente si allinea il laser affinché il fascio luminoso incida
ortogonalmente sulla superficie della vasca. A tal proposito si verifica che
con l'angolo di calibrazione sul nonio ad arco a grandi distanze il
raggio punti sempre sullo stesso punto e non subisca rifrazione (per
$\theta_i=0$, vale $\sin{\theta_i}=0$).

Per ognuno dei mezzi presi in considerazione vengono prese dieci misure in un
angolo totale di circa $\ang{30}$. Per ogni misura si varia la rotazione del
nonio cambiando dunque l'incidenza del raggio laser sul mezzo, e si segna sul
foglio millimetrato la traccia del raggio sulla carta. La misura della distanza
tra le rette segnate (equidistanti perché parallele) avviene con l'uso di un
righello. La misura della profondità della vasca contenente i liquidi viene
eseguita con l'uso del nonio lineare.

\label{sec:setup}

\section{Descrizione dei risultati}
    I dati delle distanze sono ottenuti facendo la media su 5 misure diverse per ogni retta, da ciò ne deriva una diminuzione dell'incertezza di un fattore $\sqrt{5}$. Dopo aver posto $n_1 = 1$, essendo $n_1$ l'indice di rifrazione dell'aria (approssimabile a quello del vuoto), si effettuano due fit: il primo su parametri $t$ e $n$ e, dopo aver misurato $t$ con il calibro, il secondo solo su $n$. La misura di $t$ corrisponde a $\SI{81.35(5)}{\milli\meter}$.

    In seguito vengono riportati i grafici di $x$ in funzione di $\theta_i$ dell'acqua e dell'olio, rispettivamente in Figura~\ref{fig:meas_h2o} e in Figura~\ref{fig:meas_oil}.

\begin{figure}[h]
	\centering
	\includegraphics[page=1,width=.7\textwidth]{multipage.pdf}
	\caption{Dati e fit per le misure con l'acqua}
	\label{fig:meas_h2o}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[page=2,width=.7\textwidth]{multipage.pdf}
	\caption{Dati e fit per le misure con l'olio}
	\label{fig:meas_oil}
\end{figure}
    
    Con un set di $10$ dati, per fit si hanno $8$ gradi di libertà nel primo caso e $9$ nel secondo. Con un intervallo di accettazione $\chi^2$ tra $[0.05,0.95]$ otteniamo rispettivamente i seguenti intervalli di accettazione del $\chi^2$ ridotto:
    $$\chi^2 (8\text{dof}) \in [0.34,1.94] \qquad \chi^2 (9 \text{dof}) \in [0.37,1.87]$$

    Per l'acqua si ottengono i seguenti risultati per il primo fit:
    $$\chi^2 = 1.38 \qquad t = \SI{56(41)}{\milli\meter} \qquad n_{H_2O} = \SI{1.6(7)}{};$$
    mentre il secondo fit restituisce:
    $$\chi^2 = 1.43 \qquad n_{H_2O} = \SI{1.32(1)}{}.$$

    Dal primo fit non si ottiene una buona stima nè di $t$ nè $n$ in quanto in entrambi i casi l'errore relativo supera ampiamente il $40\%$. I risultati rimangono comunque compatibili rispettivamente con la misura $t$ ottenuta con il calibro e con i valori tabulati di $n$ alla lunghezza d'onda considerata (\href{https://refractiveindex.info/?shelf=main&book=H2O&page=Hale}{refractiveindex.info}). La stima di $n$ ottenuta grazie al secondo fit è più precisa in quanto l'errore relativo corrisponde allo $0.75\%$ e il valore di $n$ continua a essere compatibile con i valori tabulati.

    Per l'olio si ottengono i seguenti risultati per il primo fit:
    $$\chi^2 = 1.64 \qquad t = \SI{81(42)}{\milli\meter} \qquad n_{olio} = \SI{1.6(5)}{};$$
    mentre il secondo fit restituisce:
    $$\chi^2 = 1.64 \qquad n_{olio} = \SI{1.60(2)}{}.$$

    Anche per l'olio vale un discorso analogo allo stesso fatto sui risultati ottenuti con l'acqua, mentre in questo caso risulta impossibile il confronto con i valori tabulati in quanto la composizione dell'olio risulta sconosciuta.

    Inoltre si studio la bontà dell'approsimazione a raggi parassiali grazie all'equazione~\ref{eq:liltheta}, ponendo $n_1 = 1$ e $n_2$ uguale ai valori ottenuti precedentemente con i fit ad un parametro. Osservando il grafico si nota come l'approssimazione sia buona fino ad angoli di $\simeq \ang{20}$, valore per cui si compie un errore rispetto al modello precedente del $6.30\%$ nel caso dell'acqua e del $4.62\%$ nel caso dell'olio.

\section{Conclusione}
È stata verificata la validità delle Leggi di Snell, verifcando l'equazione~\ref{eq:fitmodel}. È possibile così stimare gli indici di rifrazione dell'acqua e dell'olio. La stima risulta più precisa quando si effettua il fit solo sull'indice di rifrazione, essendo il parametro dello spessore noto per vie sperimentali. I valori ottenuti con questo metodo sono:
    $$n_{H_2O} = \SI{1.32(1)}{} \qquad n_{olio} = \SI{1.60(2)}{}$$

    È stata valutata l'approsimazione per raggi parassiali o per piccoli angoli di incidenza, che risulta essere buona per angoli minori di $\ang{20}$.
    %Si riassumono i risultati ottenuti con la metodica usata. Molto simile all' abstract
%ma piu' concentrati sul finale. Un minimo di discussione dei risultati ed eventuali problematiche 
%trovate. (10/15 righe al massimo)

\section{Riflessione totale}

%La stima dell'angolo di riflessione totale (a cui quindi non esiste più il
%raggio rifratto) avviene con misure ripetute simili a quelle indicate nella
%Sezione~\ref{sec:setup}. È necessaria l'accortezza di massimizzare l'uso della
%sezione della vasca, in quanto per mezzi come acqua e olio è ragionevole
%aspettarsi un angolo di riflessione totale vicino all'angolo retto: ad esempio
%si può utilizzare il lato corto della vasca, avendo la premura di verificare
%l'allineamento del raggio laser.

La stima a posteriori dell'angolo di riflessione totale la possiamo fare
utilizzando la seguente equazione:
\begin{equation}
	\theta_{\text{crit}} = \arcsin{\frac{n_2}{n_1}}.
	\label{eq:angcrit}
\end{equation}

Il fenomeno della riflessione totale avviene per impostazioni tali per cui
$n_2<n_1$. Data la $n_{\text{pg}}=1.4906$ del plexiglas tabulata, si nota che
$n_{\text{olio}}>n_{\text{pg}}>n_{\text{aria}}$. Si stima dunque che l'angolo di
rifrazione totale sia il seguente sulla membrana plexiglass-aria:
\begin{equation}
	\theta_{\text{crit}}=\arcsin{\frac{1}{n_{\text{pg}}}}\approx \ang{42.13}.
	\label{eq:angcrit1}
\end{equation}
L'angolo di incidenza dovrebbe essere, considerando la membrana olio-plexiglas:
\begin{equation}
	\theta_i^{\text{crit}} = \sin{\theta_{\text{crit}}}
	\frac{n_{\text{olio}}}{n_{\text{vetro}}} \approx \ang{46.06}.
	\label{eq:angcrit2}
\end{equation}

\end{document}
